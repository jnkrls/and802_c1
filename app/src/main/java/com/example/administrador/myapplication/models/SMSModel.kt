package com.example.administrador.myapplication.models

data class SMSModel(
    var id:Int,
    var fecha:String,
    var de:String,
    var contenido:String
)