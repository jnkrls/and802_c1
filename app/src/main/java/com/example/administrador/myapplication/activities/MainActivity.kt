package com.example.administrador.myapplication.activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.example.administrador.myapplication.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
