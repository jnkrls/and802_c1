package com.example.administrador.myapplication.activities

import android.Manifest
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.example.administrador.myapplication.R
import com.example.administrador.myapplication.adapters.ReadSMSAdapter
import com.example.administrador.myapplication.models.SMSModel
import kotlinx.android.synthetic.main.activity_read_sms.*
import pub.devrel.easypermissions.EasyPermissions
import pub.devrel.easypermissions.AfterPermissionGranted
import java.text.SimpleDateFormat
import java.util.*


class ReadSMSActivity : AppCompatActivity(), EasyPermissions.PermissionCallbacks {
    val adapter = ReadSMSAdapter()

    override fun onPermissionsDenied(requestCode: Int, perms: MutableList<String>) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onPermissionsGranted(requestCode: Int, perms: MutableList<String>) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        EasyPermissions.onRequestPermissionsResult(requestCode,permissions,grantResults)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_read_sms)

        lvDatos.adapter = adapter
    }

    override fun onResume() {
        super.onResume()

        btnSolicitarPermiso.setOnClickListener{
            solicitarPermisoSMS()
        }
    }

    companion object {
        const val RC_SMS = 123
    }

    @AfterPermissionGranted(RC_SMS)
    private fun solicitarPermisoSMS() {
        val perms = arrayOf<String>(Manifest.permission.READ_SMS)
        if (EasyPermissions.hasPermissions(this, *perms)) {
            // En caso el permiso ya este autorizado
            obtenerSMS()
        } else {
            // En caso el permiso este denegado o ha sido
            EasyPermissions.requestPermissions(
                this, getString(R.string.mensaje_permiso),
                RC_SMS, *perms
            )
            //El permiso es requerido
        }
    }

    private fun obtenerSMS() {
        //Armamos query para obtener los SMS
        var cursor = contentResolver.query(
            Uri.parse("content://sms"),
            arrayOf("_id","address","date","body"),
            null,
            null,
            "date desc")


        var lista = ArrayList<SMSModel>()

        while(cursor.moveToNext()){

            val fecha = cursor.getString(cursor.getColumnIndex("date"))
            val dateFormat = SimpleDateFormat("dd/MM/yyyy HH:mm:ss")

            //Creamos un objecto de tipo SMSModel
            val smsModel = SMSModel(
                cursor.getInt(cursor.getColumnIndex("_id")),
                dateFormat.format(Date(fecha.toLong())),
                cursor.getString(cursor.getColumnIndex("address")),
                cursor.getString(cursor.getColumnIndex("body"))
            )
            lista.add(smsModel)
        }

        adapter.agregarDatos(lista)


    }



}
