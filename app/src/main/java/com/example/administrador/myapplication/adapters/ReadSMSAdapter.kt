package com.example.administrador.myapplication.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.example.administrador.myapplication.R  // R import
import com.example.administrador.myapplication.models.SMSModel
import kotlinx.android.synthetic.main.item_sms.view.*

class ReadSMSAdapter : BaseAdapter(){
    var lista = ArrayList<SMSModel>()

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view = LayoutInflater.from(parent!!.context).inflate(R.layout.item_sms, parent, false)

        //Obtenemos los datos
        val item = getItem(position) as SMSModel //o el getItem cambiar tipo any a SMSModel

        //Añadimos los datos al diseño
        view.tvContenido.text = item.contenido
        view.tvDe.text = item.de
        view.tvDia.text = item.fecha

        return view

    }

    override fun getItem(position: Int): Any {
        return lista[position]
    }

    override fun getItemId(position: Int): Long {
        return lista[position].id.toLong()
    }

    override fun getCount(): Int {
        return lista.size
    }

    fun agregarDatos(datos:ArrayList<SMSModel>){
        //Limpiar datos de la lista previamente
        lista.clear()
        lista.addAll(datos)
        notifyDataSetChanged()
    }

}